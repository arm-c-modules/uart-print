/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/05/31
 *===========================================================================*/

/*=====[Avoid multiple inclusion - begin]====================================*/

#ifndef _UARTPRINT_H_
#define _UARTPRINT_H_

/*=====[Inclusions of public function dependencies]==========================*/

#include "sapi.h"

/*=====[C++ - begin]=========================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*=====[Definition macros of public constants]===============================*/

#ifndef elementsof
	#define elementsof(x)  (sizeof(x) / sizeof((x)[0]))
#endif

/*=====[Public function-like macros]=========================================*/

/*=====[Definitions of public data types]====================================*/

/*=====[Prototypes (declarations) of public functions]=======================*/

void uartWriteArraytoString (uartMap_t uart, uint32_t * vector, uint32_t longitud);

void uartWriteArraytoStringLF (uartMap_t uart, uint32_t * vector, uint32_t longitud);

void uartWriteInttoString (uartMap_t uart, int32_t integer);

/*=====[Prototypes (declarations) of public interrupt functions]=============*/

/*=====[C++ - end]===========================================================*/

#ifdef __cplusplus
}
#endif

/*=====[Avoid multiple inclusion - end]======================================*/

#endif /* _UARTPRINT_H_ */

