/*=====uartPrint===========================================================
 
/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/05/31
 *===========================================================================*/

/*=====[Inclusion of own header]=============================================*/

#include "uartPrint.h"

/*=====[Inclusions of private function dependencies]=========================*/

/*=====[Definition macros of private constants]==============================*/

/*=====[Private function-like macros]========================================*/

/*=====[Definitions of private data types]===================================*/

/*=====[Definitions of external public global variables]=====================*/

/*=====[Definitions of public global variables]==============================*/

/*=====[Definitions of private global variables]=============================*/

/*=====[Prototypes (declarations) of private functions]======================*/

/*=====[Prototypes (declarations) of external public functions]==============*/

extern char* itoa(int value, char* result, int base);

/*=====[Implementations of public functions]=================================*/

void uartWriteArraytoString (uartMap_t uart, uint32_t * vector, uint32_t longitud) {
   uint8_t uartBuff[10];
   uint32_t i = 0;
   uartWriteString( uart, "{ " );
   for(;i < longitud - 1;){
	   itoa( vector[i], uartBuff, 10 ); // base 10 significa decimal
	   uartWriteString( uart, uartBuff );
	   uartWriteString( uart, ", " );
	   i++;
   }
   itoa( vector[i], uartBuff, 10 ); // base 10 significa decimal
   uartWriteString( uart, uartBuff );
   uartWriteString( uart, " }" );
}

void uartWriteArraytoStringLF (uartMap_t uart, uint32_t * vector, uint32_t longitud) {
   uint8_t uartBuff[10];
   uint32_t i = 0;
   uartWriteArraytoString(uart, vector, longitud);
   uartWriteString( uart, "\r\n" );
}

void uartWriteInttoString (uartMap_t uart, int32_t integer) {
   uint8_t uartBuff[10];
   itoa( integer, uartBuff, 10 ); // base 10 significa decimal
   uartWriteString( uart, uartBuff );
}
/*=====[Implementations of interrupt functions]==============================*/

/*=====[Implementations of private functions]================================*/

